#!/bin/bash

url='https://nouveau.freedesktop.org/'
outdir=$1

echo '/wiki/ / 301' >> $outdir/_redirects
echo '/wiki/* /:splat 301' >> $outdir/_redirects
echo "$url" >> $outdir/sitemap.txt
echo "sitemap: ${url}sitemap.txt" >> $outdir/robots.txt

while read line; do
	path=${line%.*}
	path_no_out=${path#$outdir}
	file=${line##*/}
	# we have to create the paths so gitlab pages doesn't do stupid
	mkdir -p "$path"

        echo "/$path_no_out/index.html /$path_no_out.html 301" >> $outdir/_redirects
        echo "/$path_no_out/ /$path_no_out.html 301" >> $outdir/_redirects
	echo "$url$path_no_out.html" >> $outdir/sitemap.txt
done < <(find $outdir -type f -iname "*.html")
