[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_30]]/[[ES|Nouveau_Companion_30-es]]/[[FR|Nouveau_Companion_30-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 11 de noviembre


### Introducción

Y, de nuevo, han pasado más de 2 semanas y ya es hora para otro número del TiNDC. 

Estamos ahora peleando por tener una primera versión "estable" de nouveau lo antes posible. Esta versión incluiría 2D, Xv y aceleración EXA para todas las tarjetas, desde la NV05 a la NV4x. Quienes tengan una NV04 no tendrán aceleración EXA debido a las limitaciones del hardware, mientras que las tarjetas NV5x (GeForce 8x00) no tendrán mucho más que un 2D funcional y, esperamos, establecimiento de modo para los casos más comunes. 

NV5x está en seria falta de desarrolladores, con sólo Darktama (que no puede dedicar demasiado tiempo debido a compromisos en la vida real) y dos desarrolladores noveles que intentan hacerse cargo. KoalaBR está prácticamente limitado a algún trabajo de ingeniería inversa y a hacer pruebas, debido a cuestiones de la vida real. 

A menudo obtenemos informes de que nuestro driver funciona con lentitud en Gnome o KDE, en los que el dibujado de los iconos parece producirse con retraso. 

Todos los casos vistos hasta el momento acaban en instalaciones de xserver 1.3. El paso a una versión más reciente, xserver 1.4.1, soluciona el problema. Por favor, no uséis la versión xserver 1.4 ya que tiene algunos problemas con EXA y los controladores de entrada. 

Por cierto, en cada nueva edición del TiNDC molesto a los diferentes desarrolladores para que detecten errores tipográficos y de otro tipo. Querría darles las gracias por su ayuda. 


### Estado actual

Marcheu solucionó los problemas pendientes con EXA en las NV3x e incluso logró hacer que funcionase el controlador en los PPC. Sin embargo, todavía existen algunos problemas al utilizar composite, que producen la desaparición de texto en aplicaciones QT. 

Lo siguiente que queremos hacer es decir "Gracias" a [[IronPeter|IronPeter]] que está trabajando para que funcione el 3D en la PS3 (una máquina big endian). Solicitó ayuda y, hasta donde podemos, estamos encantados de ofreceral. Hizo funcionar programas de vértices 3D y programadores de fragmentos (fragment shader). Lo mejor es que también localizó nuestro problema con los colores eliminados en glxgears: los fragment shaders necesitan conmutar las constantes de 32bits en bloques de 16bits (ABCD sería CDAB). Darktama modificó inmediatamente nuestro código para DDX para reflejar esa cuestión y airlied hizo algunas pruebas, con éxito, en su G5. !Así que ahora también tenemos EXA acelerado en PPC! 

A la vista de eso, airlied añadió una correción del fallo al driver DRI que logró resolver el problema de los engranajes (gears) sin colores. 

Las pruebas posteriores en distinto hardware fueron de resultados dispares para 2D: algunos decían que funcionaba, otros que fallaba.  
 [[Blog de airlied|http://airlied.livejournal.com/52707.html]]   
 [[IRC|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-11-01.htm#0758]]  
 [[Parche de DRI|http://gitweb.freedesktop.org/?p=mesa/mesa.git;a=commit;h=ee793281b221415f794af6aadaa9764023612e0b]]  
 [[Parche de DDX|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=f59e596bcd90ef824cd41e0c37952e574d6914bb]]  
 [[Parche de DDX|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=5bd8ba52788b3b3b8f91ba59c29e154e38482481]] 

Pero aún así, EXA en tarjetas distintas a la NV4x o no funcionaba o lo hacía con mucha lentitud. Por lo que ambos, Airlied y Marcheu, se acercaron al gurú de PPC, benh, y le pidieron ayuda. Se ofreció a echar un vistazo al código y auditarlo para localizar fallos habituales de orden de bits. Todavía está en marcha esa tarea, pero al menos parece que no cometimos ningún crimen obvio para PPC, ya que incluso benh necesita echarle un vistazo en profundidad. 

AndrewR probó EXA en NV11 e informó del mismo problema (fuentes que desaparecen). p0g investigó el asunto y quedó perplejo por los valores de posición X/Y pasados a nouveau por EXA. Más tarde localizó el problema y lo arregló. 

Ahuillet y p0g intentaron solucionar el último impedimento para obtener una aceración EXA rápida en NV1x: encontrar una forma de implementar la operación [[PictOp|PictOp]] A8+A8. Se vió que era una cuestión complicada, ya que no podían encontrar una manera que fuese soportada por el hardware. Se debatieron varias optiones (como la conversión a un formato conocido, dónde convertir, etc), pero no surgió ninguna solución inmediata. Tras un par de pruebas que resultaban problemáticos si el ancho o las posiciones de destino x/y no eran pares, se inició un debate entre ahuillet, p0g y marcheu. Los hacks de ahuillet se abandonaron y se esbozó un parche mejorado para EXA.   
 [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-07#1848]] 

Se intentaron probar valores distintos en el formato de textura para ver si así se localizaba un modo adecuado de textura, pero sin éxito. 

Pmdata y CareyM localizaron fallos en renouveau, que producían pérdida de memoria / errores / resultados incorrectos en algunos de los test. CareyM incluso retrocedió a las versiones 71xx del controlador para asegurarse de que no se trataba de un error en el mismo (¡Gracias por este trabajo árduo!). Más tarde pmdata localizó al culpable: un Enable(GL_TEXTURE_RECTANGLE_NV) donde no era necesario. Reescribió el test para eliminar el fallo.   
 [[Parche|http://nouveau.cvs.sourceforge.net/nouveau/renouveau/tests.c?r1=1.216&r2=1.217]] 

stillunknown, junto con otros colaboradores, intentó poner en forma el cambio de modo. chowmeined tenía problemas con los tiempos que producía un par de líneas rosas en la parte inferior y derecha de la pantalla. Logró eliminar las de la parte inferior pero no consiguió deshacerse completamente de las de la zona derecha. Stillunknown le mandó más parches de prueba hasta lograr solucionarlo. 

Más adelante malc0 se unió al escenario con un parche que interpretaba el DCB en la bios de la tarjeta. Además hizo una limpieza en el código DDX y de randr 1.2 para facilitar el desarrollo y el mantenimiento posterior. 
[[!table header="no" class="mointable" data="""
**Inserción: Bloque de Configuración de Pantalla (DCB)**  
El DCB es una parte de la bios de la tarjeta de vídeo que consiste en una serie de entradas que incluyen datos interesantes para el establecimiento del modo (de vídeo). Cada entrada está relacionada con una salida potencial (y posible) como VGA, TV-OUT, etc. Actualmente nouveau usa los datos asociados con las entradas para saber qué tipo de salidas están presentes, qué puertos I2C usar para obtener datos EDID para cada salida, y cómo se debería realizar el enrutado del RAMDAC.
"""]]

Posteriormente stillunknown logró iniciar en frío la segunda cabeza de salida para usarla con randr1.2, con lo que se resuelve un problema importante más.   
 [[Parche|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=8241710c94f6df0bf683bc3c93f7ea1ca14d118c]]  
 [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-06#1030]] 

pq hizo algunas pruebas en NV20 y aportó más información sobre sus resultados. Jugando con algunos registros de tiempos, logró hacer que funcionase perfectamente la NV20.   
 [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-03#2042]] 

jb17some presentó su trabajo de ingeniería inversa de XvMC. Parece estar seguro de que ahora comprende el funcionamiento básico del sistema. La FIFO configura y controla la visualización, como es habitual, pero los datos no se envían vía FIFO, sino a través de un buffer independiente. Este buffer se crea y actualiza a través de llamadas a XVMCCreateContext(), XVMCCreateSurface(), XVMCRenderSurface(). Todavía no está claro si va a probar a hacer una implementación.   
 [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-03#2232]] 

Acabado el trabajo en NV3x y con marcheu solucionando algunos de los problemas restantes en NV30, jkolb fijó su mirada en un nuevo reto. Después de hablar con marcheu, airlied y darktama se lanzó a trabajar un poco en Gallium y TTM. Jkolb hizo un par de cambios. Primero un esqueleto de funciones de controlador BO que no hacía nada, y luego le añadió algo del código TTM obtenido del de radeon, hecho por airlied.  En ese punto, darktama le mandó a jkolb un archivo de diferencias con su rama TTM, y le añadió parte del código que faltaban en el controlador BO, para luego incorporar el driver de darktama para implementar las barreras (fence driver). En ese punto, darktama trabajó en solucionar algunos errores que no se producían con el código original. 

Todavía falta mucho para obtener una plataforma completa de Gallium3d / TTM. Todavía faltan algunas piezas fundamentales en el DRM ("superioctl", algunos fallos importantes, etc, etc), y todavía hay que adaptar todo el código en espacio de usuario. ¡Eso es todavía muchísimo trabajo! 

Por cierto, airlied ha enviado la última versión del DRM, que incluye TTM, a la LKML (lista de correo del kernel Linux). Eso significa que la API debería estar tan acabado y estable como debe en Linux.   
 [[Mensaje a la LKLM|http://www.uwsg.iu.edu/hypermail/linux/kernel/0711.0/1178.html]] 

Por lo que ahora "sólo" necesitamos rellenar las partes que controlan los sistemas 3D de las tarjetas. Esto es más fácil de decir que de hacer, ya que marcheu necesita terminar su parte genérica de GAllium3D para que las tarjetas antiguas, sin programadores de longitud variable, puedan manejar las peticiones correctamente. 

Algunos temas cortos: 

* Estamos buscando a una persona que escriba un test para nouveau para contestar la pregunta "¿Funcionan los notificadores en espacio AGP?". Si no funcionasen habría que usar notificadores en el espacio PCI. Tanto pq como marcheu están ocupados, pero este parche ayudaría a avanzar un poco. 
* nuestra política respecto al estilo del código es: indentación con tabs de ancho 8 caracteres más espacios en blanco adicionales si la indentación es menos a 8 caracteres (contados desde el último tabulador en la línea). 
* La documentación de Xv de los registros de mmio-parse register fue mejorada por hkBst. 

### Ayuda necesaria

Por favor, probad si funciona nouveau en PPC (si tenéis ese hardware). 

Además, echad un vistazo a la página [["Testers wanted"/"Se buscan Probadores"|http://nouveau.freedesktop.org/wiki/TestersWanted]] para las solicitudes que surgen entre número y número. 

Y, finalmente, gracias a Luc Vernerey por enviarnos 2 tarjetas (GeForce 3 y 4). 
