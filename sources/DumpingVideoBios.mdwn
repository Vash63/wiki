# Dumping a Video BIOS

A video BIOS may contain x86 binary code, binary scripts and several data tables, which describe the hardware. Portable drivers need to parse the scripts and tables to drive the hardware correctly. Therefore driver developers sometimes would like to look into BIOSes from different cards. 

There are several options on how to dump the BIOS: 

* nvagetbios: [[https://github.com/envytools/envytools/blob/master/nva/nvagetbios.c]] 
* using any nouveau module after 2.6.34 ([[commit|http://git.kernel.org/?p=linux/kernel/git/torvalds/linux.git;a=commit;h=b42861f40f5ea3261d9fa1d5533496bf1fc812cf]]) `Note: assuming that you have debugfs mounted on "/sys/kernel/debug"`: 
     * `cat /sys/kernel/debug/dri/0/vbios.rom > vbios.rom` 

On powerpc machines, the video BIOS can be retrieved from Open Firmware: 

* find the file with:  `grep -r "IBM VGA Compatible" /proc/device-tree/` 
* copy it to a convenient location, using the path obtained above. For example on a 12" powerbook this is most likely: `cp "/proc/device-tree/pci@f0000000/NVDA,Parent@10/NVDA,BMP" ~/vbios.rom` 

Make sure you have a file of roughly 65kB in size (it may be smaller on older cards). If it's only a few bytes (or even worse, empty) the dump probably failed. 
