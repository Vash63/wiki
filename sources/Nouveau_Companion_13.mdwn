## The irregular Nouveau-Development companion


## Issue for February, 1st


### Intro

Well, the last issue of was huge, more than double the average size an issue has normally. This issue can only be shorter, so please don't be disappointed. 

phoronix.com again featured a short overview mainly over our NV4x progress. They did even test 3D / glxgears on a GForce 6150 and got it running. Impressive, as we didn't have anything in our wiki explaining howto setup the 3D part of the driver (this situation has changed since then).  Look here : [[http://www.phoronix.com/scan.php?page=article&item=634&num=1|http://www.phoronix.com/scan.php?page=article&item=634&num=1]]  and to those of you complaining the fps would be really low, please read the paragraph about NV4x testing carefully. 

And as a last good bye to the LCA 2007, here is the video of Airlied's talk: [[http://mirror.linux.org.au/pub/linux.conf.au/2007/video/wednesday/154.ogg|http://mirror.linux.org.au/pub/linux.conf.au/2007/video/wednesday/154.ogg]] 

The pledge drive is nearing the end and David Nielsen and Marcheu finally worked out the payment details. David will notify all of you who have signed the pledge within the next 2 weeks (as the pledge is ending on 08.02.2007). 


### Current status

As Ubuntu and Fedora have been showing interest in integrating nouveau in their next release and with release dates slowly approaching, marcheu tried to fix some of the most glaring bugs in the 2D driver in order to avoid getting bug reports on problems, we already know. We will be probably drowning in feedback and we will need a working bugzilla then. As already mentioned in the last issue, Marcheu is working on that. 

Regarding the crashes on G70 with the 2D driver cjb, marcheu and KoalaBR seem to have nailed the hardware bug with a software work around. But G71/G72 cards still seem to crash. 

With work on the 3D driver advancing, the requirements regarding data from reverse engineering grows. Most of the needed info is buried deep within the `nv_object[]` structure, used by renouveau to dump the known data in a meaningful way. Copying data manually between renouveau and nouveau would only lead to errors, which is why "generator" was written some months ago. The generator parses the nv_object structure and creates a header file `nouveau_reg.h` which offers macros and defines as needed by the driver code. As more data needed to be exposed, KoalaBR sat down and enhanced the version much to Darktama's and marcheu's liking. 

jwstolk analysed the data from pq in earnest creating formulae matching the output from the blob. After some hard work (with the CPU running at 0.0% idle) he finally found the correct formulae for all parameters. But the data created was optimized for easy analyzation, but didn't really reflect day to day usage. So the next step jwstolk is going to take (probably with some help by pq), is checking this formulae against "real" spotlight data. First steps have already been taken by jwstolk and pq. 

(_Corrected_) Since last issue jrmuizel, doener and inferno have been trying to fix the outstanding issues regarding kmmio. With every revision of the (which were tested again and again by pq) tool, it was inching closer to stability, which however stayed an elusive goal. Some problems were due to support for large pages while others couldn't be found. Inferno finally created support for large pages and doener had once again a bright  moment and found the reason for the crashes and fixed it. Now even newer  drivers than 8776 are working stable under kmmio. Still it isn't rock stable, it may still crash every now and then. 

(_Corrected_) This paragraph explains the reason for the crashes, you can safely skip it, should you be not interested in technical details. Each process has its own pgd (page global directory) which is the first table in a chain of lookups the processor has to do when a memory access is performed with paging enabled. For kernel mappings, there's a reference pgd that has entries for all of them. As each process has its own pgd, it needs to be synchronized with the reference one whenever access to a kernel mapping is required and no sync has happened yet. Normally, that would happen when a page fault occurs. But in our case, kmmio intercepted the page fault handler just _before_ that sync was performed and happily treated the fault as handled. That was right when you only look at the reference pgd, but the process' pgd of course was still out of sync and caused the process to fault again... and again... and again...  resulting in an endless downward spiral. Really hard to spot, but great job guys! Still some smaller issues remain, have a look at the [[MmioTrace|MmioTrace]] page, too. 

The success with mmio in turn resparked pq's interest in rules-ng in  order to enhance the ouput of mmio-trace. At first pq just wanted to replace certain values from the traces with symbolic names found in the rules.xml file from the RivaTV project. To his dismay, he found that the last revision of this file doesn't obay its DTD. Even worse, some entries are duplicated (with different values) and a hard to retrieve. So he is thinking about redesigning rules.xml into rules-ng.xml. His current thoughts (after talking with pmdata about requirements) can be found in the documentation CVS repo. 

So if you feel brave enough to test MMIO, please note that you will be on your own! As development is still progressing towards stability (knock on wood), we can offer you no further support on #nouveau. Questions will be delegated to the [[MmioTrace|MmioTrace]] page on the wiki. 

After coming back from LCA 2007 airlied started a redesign of his randr1.2 branch resulting in heavy breakage. As of January, 30th the branch is working  again. Airlied published a photo on his blog ([[http://airlied.livejournal.com/39093.html|http://airlied.livejournal.com/39093.html]]),  which I have simply stolen for your viewing pleasure: [[!img http://www.skynet.ie/~airlied/pics/nouveau_randr.jpg] 

Currently Airlied is not really looking for testers, as his work on this branch will be slow for the next few weeks. Despite that pq did some tests on 7.1.1 and got some interesting results: 

* Logical mouse pointer position differs from the position drawn. 
* Problems when a monitor doesn't report DDC values (or reports them incorrectly) resulting in incorrect monitor resolutions. 
* Some switching trickery may be needed to get a working display on both heads. 
* Screen orientation may be wrong (As if turned by 90 degrees right) 
Helpful souls often drop into our channel trying to get nouveau installed from our sources. Unfortunately (as mentioned in an earlier issue) our [[InstallNouveau|InstallNouveau]] page was out of date. That wasn't helped much by the fact that we sort of fixed it by replacing the cvs links to the git links. After one more person and even KoalaBR fell into this trap, finally KoalaBR had enough and fixed the page. (I seem to remember that KoalaBR promised to do  this last year, but well what do you expect from Koala? Slumbering ~20 hours a day...) 

Testing the newest nouveau on NV43 at first revealed further progress, the glxgears frame rate more than doubled. But the most current version locks up  for some testers. As KoalaBR was able to reproduce the lock ups, darktama asked for a git-bisect in order to find out the bad commit. After a false positive the result pointed to a commit which looked totally innocent. Marcheu and Darktama intend to have a look at it together. 


### Help needed

If you still encounter 2D crashes, please report to Marcheu or KoalaBR. If you can do kernel compiles and are not afraid to test kernel modules, please lend jrmuizel, pq or doener a helping hand. We could use some more  mmio trace for all NV4x cards. But please ask first! 

[[<<< Previous Issue|NouveauCompanion_12]] | [[Next Issue >>>|Nouveau_Companion_14]] 
