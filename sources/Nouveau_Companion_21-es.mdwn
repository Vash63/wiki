## Informe irregular sobre el desarrollo de Nouveau


## Edición del 10 de Junio


### Introducción

Bienvenido de nuevo al número 21 del TiNDC. 

Antes de empezar, querría mostrar mi agradecimiento a todos los participantes y a aquellos que han hecho pruebas en el proyecto. No sólo los desarrolladores son importantes, sino todos vosotros. Gracias especialmente a los miembros del equipo de traducción, que se toman el trabajo de traducir los TiNDCs para que estén disponibles al mismo tiempo que la edición en inglés. 

ABClinux entrevistó a Andy Ritger, Director de Software Unix en NVIDIA. Una de las preguntas hacía alusión a nouveau y esto es lo que NVidia prentende hacer respecto a nosotros: 

   * La posición de NVIDIA es: ni ayudar ni estorbar al proyecto Nouveau. Estamos comprometidos con el soporte de Linux a través de a) el driver 2D para las X de código abierto "nv", que mantienen y mejoran de forma activa nuestros ingenieros, y b) nuestro completo driver propietario para Linux, que se beneficia del código compartido con otras plataformas a las que NVIDIA da soporte. 
La entrevista completa se puede leer [[aquí|http://www.abclinuxu.cz/clanky/rozhovory/andy-ritger-nvidia?page=1]]. 

Parece que eso es lo mejor que podemos esperar en estos momentos. 

Respecto a la SFC, y, por tanto, la donación de 10.000$: la reunión tuvo lugar y los miembros del comité no estaban convencidos de si aceptarnos o no. Pidieron más información técnica a marcheu, que está ahora procesándola y escribiéndola. Esperemos tener una decisión final pronto. 

Finalmente, descubrimos que teníamos espacio asignado desde que nos trasladamos a los servidores de fd.o, y enseguida trasladamos todos los volcados de sf.net a [[fd.o|http://nouveau.freedesktop.org/tests/]]. En estos momentos estamos a la espera de una cuenta para ejecutar el script de kmeyer para realizar el almacenamiento de forma automática. No deberíamos tardar mucho en ello. 


### Estado actual

Ahora vamos a la parte que estoy seguro que es la que más os interesa, la de los avances técnicos. 

ahuillet continúa recogiendo datos importantes sobre Xv y sus requerimientos técnicos. 

Ya ha escrito algunas página en el [[Wiki|ArthurHuillet]] . En estos momentos planea iniciar los trabajos entorno al 20 de junio, y ya ha prometido mantenernos informados sobre sus avances en los TiNDCs que se publiquen mientras dure su proyecto. 

Todavía nos estamos peleando con las tarjetas basadas en 8600GT, con las que el proyecto se ha puesto manos a la obra. La mayor parte del esfuerzo se debe a Darktama, con algo de ayuda de KoalaBR. 

La tarjeta de KoalaBR ahora funciona en 2D usando la rama nv50_branch de darktama. Se corrigieron algunas omisiones sin importancia, aunque no se han publicado en el repositorio oficial por las razones que se explican más abajo. 

El regreso al modo texto produce resultados interesantes, pero obtener el modo texto no es uno de ellos :). Por lo que KoalaBR probó a tomar código de NV que usa la int10 para hacer llamadas a la BIOS. Marcheu paró esto puesto que robar es inmoral y regañó a KoalaBR por hacerlo. (Bien, para ser precisos, la razón es más técnica: es una solución que no es portable a arquitecturas distintas a Linux x86 y x86-64). 

airlied ofreció una vbetool mejorada que, opcionalmente, permitiría ejecutar llamadas a la BIOS a través de una capa de emulación e imprimiría todos los accesos a registros de la tarjeta. Sin embargo, no funcionó con el emulador. Así que, tras meditar un rato sobre qué hacer, airlied recordó otra versión de dicha herramienta y biblioteca, que simplemente mostraría con printf()los datos necesarios. Debido a las pruebas 24x7 que tenía que hacer para su trabajo, apareció un enlace pocos días más tarde. Sin embargo, todavía no fue suficiente. La fuente de la consola se estropeaba y el monitor cambiaba a modo "en suspensión" (sin señal). Un cambio manual de texto a las X y de nuevo a texto hace que la imagen vuelva, pero la fuente todavía está mal y es necesario recargarla. 

Al hablar con airlied y marcheu sobre más posibilidades, ambos recordaron un parche de NVidia al x86emu de Xorg. NVidia indicaba que era necesario para que las tarjetas G8x funcionasen correctamente. Por lo que el próximo paso es probar esa versión y ver qué pasa. 

El objetivo es localizar los accesos a los registros y escribir una pequeña herramienta que sea capaz de hacer esas escrituras y conseguir que se realice el cambio de modo. Cuando logremos hacer funcionar la herramienta en la mayoría de tarjetas G8x, entonces incorporaremos el cambio de modo en nuestro driver. POR FAVOR, NOTA: Aunque este trabajo puede ser útil para randr12, únicamente he comentado cómo cambiar de las X al modo texto, ya que ¡es sólo eso lo que estoy intentando hacer funcionar! 

Darktama cree que ha descubierto cómo funcionan algunas de las funciones 3D, como las direcciones de las funciones de sombreado (shaders), o la  gestión de memoria virtual para cada cliente OpenGL.  La memoria virtual sería muy útil para el acceso DMA simultáneo (scatter-gather) a través de PCI o PCI-e y, por supuesto, tiene un prototipo basado en TTM (que no es exclusivo para G8x). También, parece que las tarjetas G8x actuales usan indirección de 64 bits, mientras que las anteriores únicamente usan 32bits. 

Por último, jkolb_ le echó un vistado a los asuntos pendientes de las NV3x e hizo "funcionar" su tarjeta con glxgears. Sin embargo, el asunto citado en el [[TiNDC 15|Nouveau_Companion_15]] (la matriz de proyección no se aplica) acabó por aparecer, y decidió echarle un vistazo. Vio que ninguna de nuestras funciones de inicialización de las NV3x enviaba los datos necesarios, por lo que se ofreció a elaborar un parche. Marcheu confirmó lo primero afirmando que la matriz se gestiona ahora mismo a través de shaders, y por tanto sólo está disponible en NV4x. Al usar la versión de software aparece un fallo que hace que se aplique la matriz dos veces o, como en este caso, ninguna. Marcheu alegó la protección de patentes sobre esta funcionalidad tan innovadora, y prometió echarle un vistazo la próxima semana. 

Con marcheu de nuevo en marcha la semana que viene, tras entregar su tesis, volveremos a ver más avances también en el resto de arquitecturas. 


### Ayuda solicitada

Bien, los sospechosos habituales: volcados de renouveau y mmio, además de más pruebas de nouveau. 

Quienes quieran escribir algo de código tampoco serán rechazados :) 
