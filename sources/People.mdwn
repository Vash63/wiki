A number of people have contributed to nouveau over the years. This page lists some of them and the contents of their contributions. If you contributed and aren't on here, feel free to add yourself/ask to be added.

NOTE: this list is wildly incomplete, this is largely a placeholder right now for people who have their own wiki pages.

* [[StephaneMarchesin]]
* [[PekkaPaalanen]]
* [[LucasStach]]
* [[Marcin_Kościelnicki]]
* [[jb17bsome]]
* [[ArthurHuillet]]
* [[HonzaHavlicek]]
