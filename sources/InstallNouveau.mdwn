# Installing Nouveau on your Linux computer

There are two ways of installing Nouveau on your Linux computer. The most recommended way is to use your [distribution-provided packages](#distro-packages). If those are outdated or buggy, you may also [recompile Nouveau from source](#recompilation).

Nouveau is incompatible with NVIDIA's proprietary driver. If you want to use Nouveau, you first need to remove the proprietary driver from your system.

## Using your distribution's packages
<a id="distro-packages"></a>

The following information are provided to the best of our knowledge. Please seek help on your distribution's wiki if you have any problem installing or uninstalling Nouveau.

 * [ArchLinux](https://wiki.archlinux.org/index.php/Nouveau)
 * [[Debian|DebianInstall]] : Some more [[tips|DebianTips]] and a list of [[packages|DebianPackages]]
 * [[Exherbo|ExherboInstall]]
 * [[Gentoo|GentooEbuilds]]
 * [[OpenSUSE|openSUSEPackages]]
 * [[Ubuntu|UbuntuPackages]] : Some more [[tips|UbuntuTips]] are available

## Compiling Nouveau from source
<a id="recompilation"></a>

Instead of using the distribution-provided binaries, you can also recompile Nouveau yourself. There are several reasons to want to compile Nouveau from source, here are the most common ones:

 * Testing the latest version on Nouveau;
 * Bug fixing - checking if a bug in a stable version is still present in the development branches or testing a patch;
 * Contributing code - Developers are expected to test and implement new code on the development branches.

Whatever your reason is, the development team thanks you for living on the edge. Please tell us as soon as possible when you encounter a bug!

Nouveau is split into 4 main parts:

 * Nouveau DRM : The privileged part of Nouveau, running in the kernel;
 * Libdrm : A library that wraps the DRM & Nouveau IOCTLs, it simplifies memory management and command submission;
 * xf86-video-nouveau : The device-dependent part of X (DDX) for NVIDIA cards whose role is to provide 2D acceleration (EXA) and some simple video acceleration (XV);
 * Mesa : It provides 3D, GPGPU and video acceleration to open source drivers. For more information, please read [[MesaDrivers|MesaDrivers]].

### Kernel

The kernel modules required by Nouveau (`drm.ko`, `drm_kms_helper.ko`, `ttm.ko` and `nouveau.ko`) are built from a Linux kernel tree. You have several options on how to download the source code (subsections of Section 1) and how to compile it (subsections of Section 2). See also [[KernelModeSetting|KernelModeSetting]] for important KMS notes. Please, read carefully. 

[The Nouveau kernel git
repository](https://gitlab.freedesktop.org/drm/nouveau/) contains one
recommended branch: `nouveau-next`.

You may not be able to compile the DRM modules against your current kernel, if it is too old (might need an -rc kernel or even Linus' git kernel). The possible errors include build failure and kernel modules that cannot be loaded. It is recommended to build your whole kernel from this branch (Section 2.1). 

Since the DRM code is part of the kernel since 2.6.33, you can also use Linus' git kernel for Nouveau. However, the code in Linus' kernel will always be older than Nouveau's own git, and because there are no official Nouveau releases, you may encounter compatibility issues with user space parts (libdrm, DDX, Mesa).

#### Getting the source code

You have multiple choices when it comes to downloading the source code:

 * Get the full Nouveau kernel tree, with full history : Best for developers and people willing to bisect regressions;
 * Get the full Nouveau kernel tree, without history: For continuous testers with a data cap on their internet plan;
 * Get a snapshot: For occasional testers with an internet data cap (out-of-the-tree compilation only).

In order to download the full Nouveau kernel tree, please run the following command (un-comment --depth=1 if you don't want the full history):

    git clone https://gitlab.freedesktop.org/drm/nouveau.git # --depth=1

There is also an out-of-tree version of the module that you can get at:

    git clone https://github.com/skeggsb/nouveau.git

#### Full kernel compilation

This method cannot be applied if you only downloaded a snapshot of Nouveau. Please download a complete version of the Nouveau tree.

The configuration and installation parts are distribution-dependent. You can find some instructions for [ArchLinux](https://wiki.archlinux.org/index.php/Kernels/Compilation/Traditional), [Debian](http://kernel-handbook.alioth.debian.org/ch-common-tasks.html#s-kernel-org-package), [Gentoo](https://wiki.gentoo.org/wiki/Kernel/Configuration), [Ubuntu](https://wiki.ubuntu.com/KernelTeam/GitKernelBuild) or [Fedora](https://fedoraproject.org/wiki/Building_a_custom_kernel).

Please make sure CONFIG_DRM_NOUVEAU=m, otherwise Nouveau won't be compiled in the kernel.

#### Out-of-the-tree compilation

The out-of-the-tree builds are mostly used by developers or testers who recompile their kernel often with some custom patches. The out-of-the-tree build will only recompile the nouveau driver, which should be orders of magnitude faster than a full kernel recompilation. However it has to be done against a compatible kernel, since it relies on a lot of internal kernel APIs

In order to use the out-of-the-tree compilation, clone the out-of-tree module and run the following commands:

    $ cd nouveau/drm
    $ make

Then, you should insmod the nouveau.ko module which is compiled (make sure to also modprobe its deps). It is also possible to install them but this isn't recommended.

**WARNING**: To use this technique, you will be required to be running at least the same major version of the kernel as one you want to compile out of the tree. If the kernel hasn't been released yet, please compile, install and reboot a kernel compiled from the full Nouveau tree before attempting again.

#### Run-time configuration

A [[list of the module parameters|KernelModuleParameters]] is available.

### Userspace

The following tutorial will help you compile and install Nouveau in a user directory. This will lower your chances of messing up your whole system. Each package's instructions assume that you have the proper header files installed. If you're on a Debian-based distro (including Ubuntu), you can run `apt-get build-dep mesa` to make sure that this happens.

However, if you need to recompile xf86-video-nouveau and your distribution-provided libdrm isn't sufficient, you will need to set NVD to /usr and install libdrm, mesa and xf86-video-nouveau.

If you are not in this case, please source the following script (nouveau-env.sh) in every console where you need to do Nouveau development:

    DEV_HOME=$HOME/nouveau/ # change this to another location if you prefer
    NVD=$HOME/install       # change this to another location if you prefer
    LD_LIBRARY_PATH=$NVD/lib
    PKG_CONFIG_PATH=$NVD/lib/pkgconfig/:$NVD/share/pkgconfig/
    ACLOCAL="aclocal -I $NVD/share/aclocal"
    
    export DEV_HOME NVD LD_LIBRARY_PATH PKG_CONFIG_PATH ACLOCAL

Sourcing the file can be done by running command:

    $ source nouveau-env.sh

#### Libdrm

Here are the commands to compile libdrm from git:

    $ cd $DEV_HOME
    $ git clone git://anongit.freedesktop.org/mesa/drm
    $ cd drm
    $ ./autogen.sh --prefix=$NVD
    $ make
    $ make install

#### Xf86-video-nouveau

Let's now recompile Nouveau's X.org driver (DDX) from git:

    $ cd $DEV_HOME
    $ git clone git://anongit.freedesktop.org/nouveau/xf86-video-nouveau
    $ cd xf86-video-nouveau
    $ ./autogen.sh --prefix=$NVD
    $ make
    $ make install

You'll then need to tell the X-server where to look for nouveau_drv.so. To do so, please create a configuration file at /etc/X11/xorg.conf.d/01-module-path.conf containing:

    Section "Files"
        ModulePath "/same/path/as/NVD/lib/xorg/modules/drivers/"
    EndSection

**WARNING**: Please replace /same/path/as/NVD/ by the path you set for $NVD.

**WARNING2**: If you managed to break your graphical interface. You can revert back to the previous ddx by deleting 01-module-path.conf.

#### Mesa

Mesa can be compiled by running the following commands:

    $ cd $DEV_HOME
    $ git clone git://anongit.freedesktop.org/mesa/mesa
    $ cd mesa
    $ ./autogen.sh                            \
          --prefix=$NVD                       \
          --enable-texture-float              \
          --enable-gles1                      \
          --enable-gles2                      \
          --enable-glx                        \
          --enable-egl                        \
          --enable-gallium-egl                \
          --enable-gallium-llvm               \
          --enable-shared-glapi               \
          --enable-gbm                        \
          --enable-glx-tls                    \
          --enable-dri                        \
          --enable-osmesa                     \
          --with-egl-platforms=x11,drm        \
          --with-gallium-drivers=nouveau      \
          --with-dri-drivers=nouveau          \
          --enable-vdpau
    $ make
    $ make install

### LibVDPAU (optional, only useful for video decoding)

The libvdpau will look for libvdpau_nouveau.so in the same prefix as it was installed (regardless of LD_LIBRARY_PATH). We thus need to recompile libvdpau and install it to the same prefix as mesa in order for it to use the right libvdpau_nouveau.so.

    $ cd $DEV_HOME
    $ git clone git://anongit.freedesktop.org/vdpau/libvdpau
    $ cd libvdpau/
    $ ./autogen.sh --prefix=$NVD
    $ make
    $ make install

### Configuring the X server

When using Nouveau, the X-Server doesn't need configuration, the Nouveau driver should be loaded automatically. If it wasn't the case, you may want to try to force the X-server to load the Nouveau driver. To do so, please create the file /etc/X11/xorg.conf.d/20-nouveau.conf and write the following:

    Section "Device"
        Identifier "n"
        Driver "nouveau"
    EndSection

It is possible to give some options to the Nouveau DDX. Please have a look at our man page ([man 4 nouveau](http://linux.die.net/man/4/nouveau)). For more information, please follow the instructions found in the [distribution-specific section](#distro-packages).
