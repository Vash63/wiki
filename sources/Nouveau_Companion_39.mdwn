[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[TiNDC 2008|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/EN/[[ES|Nouveau_Companion_39-es]]/FR/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Issue for May, 17th


### Intro

A bit longer in the making this time as the channel was awfully quiet the last  weeks. Partly due to Marcheu being on a conference and some developers waiting for some hints or code from him. So to get at least some content for this issue, I pestered our main developers in private about their work in the last weeks. Then suddenly the pace picked up again and I was in the middle of a system (hardware) update, so that I didn't have enough time to follow the  channel. And when I tried to catch up, freedesktop.org was doing a system upgrade too... So at least one week is missing, but it took long enough to get so far, either now or never :) 

So this issue will be only touching some topics, please do check the git repository and the logs if you need additional information. 

Airlied noted in his blog that he is adding a kernel mode setting preview to FC9. That isn't by far the real deal but it can't be too far off from what is planned. Thus the move to mainline shouldn't be too far off either.  ([[http://airlied.livejournal.com/58778.html|http://airlied.livejournal.com/58778.html]]) 

And phoronix.com did a short test of that code in this article: [[http://www.phoronix.com/scan.php?page=article&item=kernel_modesetting&num=1|http://www.phoronix.com/scan.php?page=article&item=kernel_modesetting&num=1]] 

BTW: Current Fedora has Nouveau as an available package. If you see problems with that, please try current git first before reporting bugs. Nouveau is a very fast moving target and the package is already outdated. 

Other than that: Please do test (always use Randr12!) as much as possible  and do report bugs. We are very interested in your results and user experience. 

And if you still ask yourself what exactly Gallium3D is, have a look at this link: [[http://jrfonseca.blogspot.com/2008/04/gallium3d-introduction.html|http://jrfonseca.blogspot.com/2008/04/gallium3d-introduction.html]]. 

And as the question pop up on IRC about twice a month: "Will there be a *BSD" port of Nouveau?" The answer is: "Depends". A port of the current DRM and TTM is needed in order to get it working. Other than that there shouldn't be much work needed to get Nouveau working on your favorite BSD system. So please step forward if you are interested in porting, we are interested to hear from you! 


### The Current status

After some very long time, the fix for <ins>ucmpdi2 on PPC finally went into the kernel. That should fix some reoccurring question and problems with this symbol missing. 

Currently, most work is being done regarding 3D Gallium. As Marcheu was missing in action for some time, the work on card older than NV4x stalled a bit, because of some code Marcheu intended to write for NV2x and earlier cards. Nevertheless, p0g was able to get glxgears to render on NV1x. Additionally at least some of the Marcheu's NV1x work ended up in git: 

* [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8f26e975ca6341cb3366a18beb352b5cdcaee2bc|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8f26e975ca6341cb3366a18beb352b5cdcaee2bc]] up to [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8ed894bd17bd6f426a0d87f7113f23043cda3bc3|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=8ed894bd17bd6f426a0d87f7113f23043cda3bc3]] 
pmdata had to do some work on other projects too, but still managed to add a few patches to the NV3x code path. [[http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=7b389f8d2f307fa0714494f2a43e9141cc04ed3e|http://gitweb.freedesktop.org/?p=nouveau/mesa.git;a=commit;h=7b389f8d2f307fa0714494f2a43e9141cc04ed3e]] 

Ok, seems that this time we struck gold in Google's Summer of Code. Although traditionally  late, we managed to recruit ymanton. He will work on video decoding via shader instructions. To be prototyped on Gallium3D softpipe implementation first, it will be later implemented for Nouveau. Please note that this interface will be generic, that is, other Gallium drivers will be able to use the reference implementation too. To test whether the idea is usable, Nouveau will see an implementation using graphics hardware (while softpipe is software only). 

So let's talk a bit about mode setting on NV5x cards. As you may know, we do have some problems there. The hardware is totally different from what was used up until NV4x. 

Stillunknown did try again and again, more often than not coming up with nothing usable. Finally, he noticed that the blob did read out some registers which were never set / written  to. After checking back with pq whether either he or MMio had made an error when MMioTracing,  but that turned out to be unlikely. 

So the question was up, how was the mode set? The suspicion was building that the mode setting was done via a reserved FIFO. That idea was supported by the fact that Darktama was yet unable to get FIFO 0 to work on NV5x cards. 

So independently, he came to a similar conclusion: FIFO 0 was used for mode setting. ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2008-04-27#1955|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2008-04-27#1955]]) 

Pursuing that idea further stillunknown redid the mode setting code for NV5x during a vacation and thus was rarely seen active on IRC. He committed everything and demanded KoalaBR to test the new code on a 8600 GT. Unable to come up with a good excuse for not doing so, KoalaBR obliged and reported success. Everything that worked with the old NV50 code does work with the new one too. Still, switching text mode is not yet possible. 

Speaking of mode setting: We had problems with DVI NV2x and NV3x based cards. That was due to the fact that those cards had external DVI driving chips while newer cards have those feature on chip. Malc0 fixed a few things and at least some NV2x/NV3x cards started working with DVI.  

So and just before I finished my draft, malc0 switched Randr1.2 to be default. So, please do test, if you have problems with mode setting please report and do check whether the old code has the problem or not (by deactivating Randr1.2 explicitly). 

Speaking of Darktama, work on Gallium3D hasn't stalled either. Standard work, which I never mention is tracking the progress of the Gallium main repository. That  work means merging, resolving conflicts and changing our interfaces according  to the changes which happened there. 

Additionally, Darktama added a very basic TTM interface which is mostly working although it is very slow currently. Reason is that the buffer management is extremely  simplistic for now. Now why is the code slow, is it due to generic problems in the TTM code or is it our code? Let's here what Darktama has to say about it: "It's in my gallium buffer code, I ripped it out and replaced it for TTM very quickly, and lost a heap of optimization work that existed previously some of them are fairly critical for performance :)" 

Ah yes and the problems with reflections in Neverball are solved too. First Darktama and the Gallium team thought they missed an important design detail, making reflections hard / cumbersome to implement. Well, they just overlooked one option and everything was easy to implement. 

Well the merge window for kernel 2.6.26 was closed and MMio didn't make it. That was not due MMioTrace not being worked on by pq (have a look at his MMioTrace page in the wiki, where he is constantly updating the current status), but due to the sheer volume of changes ftrace inclusion was delayed into the 2.6.27 time frame. Once ftrace goes in though, we will see pq's shiny new implementation of MMioTrace, so stay tuned. 

And jb17some continued his work on XvMC. His decoding library seems to  work for NV4x although a few hacks to be ironed out later remain. The library was imported into Sourceforge CVS and his current status can  always be found here: [[http://nouveau.freedesktop.org/wiki/jb17bsome|http://nouveau.freedesktop.org/wiki/jb17bsome]] CVS:http://nouveau.cvs.sourceforge.net/nouveau/nv40_xvmc/ 

And finally: On the third try we finally got a slot via Xorg in the Google Summer of Code. Ymanton is working on creating a video decoding facility on top of Gallium3D. A reference version will be implemented with softpipe (Gallium3D software renderer) while at the end the feature will be put to the test by implementing it for Nouveau. So a warm welcome and good luck to ymanton! See his blog about this project here: [[http://www.bitblit.org/gsoc/gallium3d_xvmc.shtml|http://www.bitblit.org/gsoc/gallium3d_xvmc.shtml]] 


### Help needed

We still need feedback on our new Randr1.2 code. As mentioned above, it is now default but we are still looking for feedback. Please test and report your results to either stillunknown or malc0. 

[[<<< Previous Issue|Nouveau_Companion_38]] | [[Next Issue >>>|Nouveau_Companion_40]] 
