# Troubleshooting Nouveau

The troubleshooting guide below is generic and not specific to any distribution. Linux distributions may have their own troubleshooting guides, which are easier to work with, if you use the distribution and its features (packages, configuration helpers, etc.) instead of the manual work assumed below.

[[!toc startlevel=2 levels=2]]

## The Basic Questions

If you have any problems with Nouveau, it does not work, it is slow, Xv is not available, or whatever, check these things first, please. 

1. Are you clear of other kernel drivers that break Nouveau? 
     * Some kernel drivers make Nouveau misbehave and must not be used, e.g.: `nvidia.ko` (the proprietary driver), rivafb, nvidiafb - more information can be found in [[KernelModeSetting|KernelModeSetting]]. Look in your kernel log and `lsmod` output for any sign of these and disable them. If you cannot find a kernel module anywhere, but it still magically loads, maybe it is in your initramfs. 
1. Is the Nvidia proprietary driver not interfering with Nouveau? 
     * Make sure you do not have the proprietary driver installed. Files, that (may) come from the proprietary driver, are: `nvidia.ko`, `nvidia_drv.so`, `libGL.so`, `libGLcore.so`, `libglx.so`. Uninstall the proprietary driver, and if necessary, reinstall Xorg (`libglx.so`) and Mesa (GL libraries) to get back the standard libraries. The X log (usually `/var/log/Xorg.0.log`) can tell you, if some of the loaded components come from Nvidia. No component should come from the vendor Nvidia. Also see Step 1 for `nvidia.ko`. 
1. Did you compile fbcon into the kernel, or compile it as a module and loaded it? 
     * First make sure you have `CONFIG_FRAMEBUFFER_CONSOLE` enabled in your kernel configuration. If it is a module (it is called `fbcon.ko`), make sure it is loaded. Otherwise activating KMS will make your console screen unusable, but your system should still work otherwise, including X. 
1. Is X using the right driver (DDX)? 
     * Do not use `nv` driver in X. Use `nouveau` instead. Check your `Xorg.0.log` file and make sure you see lines like `NOUVEAU(0)`. If you do not use such lines, check your `xorg.conf`.
1. Is your X configuration sane?
     * Try moving away your `xorg.conf` file entirely.
     * Try the minimalistic 4-line `xorg.conf` file from [[InstallNouveau]] page. That will solve most of the X configuration errors, bad monitor display modes, bad frequencies, etc. 
1. Are you using the latest code? 
     * Make sure you're using the latest available versions of nouveau's various components. See the [[front page|index]] for a list of the ~latest versions. If your distribution does not provide these as packages, see [[InstallNouveau]] to build these yourself.


## Frequently Encountered Problems

### Xorg fails to start with "(EE) [drm] failed to open device"

Your DDX does not work with your current kernel and/or libdrm. There are at least three possible reasons for this: the nouveau DRM kernel module is not loaded, a version mismatch between the Nouveau DRM and libdrm, or [[KMS|KernelModeSetting]] being disabled. 

First check, that `lsmod` command lists `nouveau`. If not, do `modprobe nouveau` to load the nouveau DRM kernel module, and check the kernel log for possible errors. 

If the kernel module `nouveau` is loaded according to `lsmod` command, but there are no kernel messages about nouveau at all, then most likely Nouveau KMS is disabled. This prevents the driver to work at all. Possible places to disable KMS are: 

* kernel command line with: `nomodeset`, `drm.modeset=0`, `nouveau.modeset=0` or similar 
* modprobe.conf or modprobe.d/ adding the module options `drm modeset=0` or `nouveau modeset=0` 
* initramfs carrying the above module options

Check and remove all those that you may find, and retry. 


### Inserting nouveau.ko fails

`drm.ko` needs to be inserted first. Make sure you are inserting the `drm.ko` that was built at the same time as `nouveau.ko`. Your kernel might provide another `drm.ko` which is incompatible. 


### Undefined symbol "GlxSetVisualConfigs"

`X: symbol lookup error: nouveau_drv.so: undefined symbol: GlxSetVisualConfigs`  
 You may need to `Load "glx"`, or you may still have the nvidia GLX driver installed, and need to use the xorg one instead. Often the nvidia module will be /usr/lib/xorg/modules/libglx.so, with the original xorg glx in /usr/lib/xorg/modules/extensions/libglx.so, in which case you can just move the nvidia .so out of the way. 


### Kernel mode setting or X sets a bad or non-native display mode

This could be due to your monitor not offering valid or any EDID information. The information is normally read using DDC. If the command `xrandr --prop` does not mention EDID for your monitor (output), then this is the case. Only a conservative default set of modes is available without an EDID. 

One option is to provide a custom EDID. You can do this by creating the directory `/lib/firmware/edid` (if it does not exist) and placing your custom edid file(s) there. Then add `drm.edid_firmware=connector1:edid/your_edid_1.bin,connector2:edid/your_edid_2.bin,...` to the kernel command line. That will make the system use `your_edid_1` for whatever is attached to `connector1` and so on. As an example, `drm.edid_firmware=DP-1:edid/foo.bin` will use `/lib/firmware/edid/foo.bin` for whatever is attached to DisplayPort 1.

Another option is to create a modeline with the command `cvt`, and apply it using the commands `xrandr --newmode` followed by `xrandr --addmode`, and set it with `xrandr --output`. See `man cvt` and `man xrandr` on how to use these commands. See [[Randr12|Randr12]] and also [[here|http://wiki.debian.org/XStrikeForce/HowToRandR12]] for information on how to configure the same to into `xorg.conf` after you have found a suitable setting. If you have no `xorg.conf`, it may suffice to create one containing only the appropriate "Monitor" section.

For the framebuffer console, you can use the `video=` kernel parameter, for instance `video=1280x1024`. For more information, see [[KernelModeSetting|KernelModeSetting]]. 

Some kernels, possibly 2.6.28 and maybe even 2.6.29, contain a bug, that causes the DDC communication to randomly fail sometimes. In that case it is recommended to update your kernel. Updating DRM modules is not enough. 


### I get the "EQ overflowing" error

"EQ overflowing" means the X server's input event queue is overflowing. It is not a bug, it is a symptom of something getting stuck. For a better explanation about what it is, see [[http://marc.info/?l=fedora-devel-list&m=124101535025331&w=2|http://marc.info/?l=fedora-devel-list&m=124101535025331&w=2]]. The most common cause is that the GPU has locked up, and eventually crashes somewhere in nouveau_exa.

When diagnosing your problem, you need to get a backtrace from the X server. At minimum, provide the complete X log, which has a backtrace. See [[Bugs|Bugs]]. 


### The computer hangs or crashes

See [[HangDiagnosis|HangDiagnosis]] for suggestions on what to do. The most important thing is to try to get the errors (hopefully) reported by the kernel. Otherwise diagnosing it is near impossible. For regressions, bisecting (with git) might provide valuable information. 


### Black, garbled, or otherwise useless framebuffer console display

This is usually due to framebuffer driver hand-over failure. See [[KernelModeSetting|KernelModeSetting]] about hand-over and check your kernel configuration and logs as instructed. 


### Blank monitor, flicker, snow, or other random live image corruption

If you use dual-head (two video outputs in use) or dual-link (a very high resolution monitor), and the VBIOS has too low clocks by default, the video memory bandwitdh will be insufficient for scanout. This can lead to a blank image, or live image corruption, that changes on every vertical refresh, and likely does not show up in screencaptures. For the monitor, this is not a case of an unsupported video mode but random video data. 

Raising the card performance mode might help. Ask on IRC, #nouveau channel, how to do that. Instructions are not given here, because in the worst case, it may destroy your card, because power management is still a work in progress. 


### My custom video mode is not effective

If you use a digital output like DVI, Nouveau will use the monitor's native mode and convert your custom video mode to that, either by stretching or adding black borders. This is undesirable, if you really want to have your custom video mode (e.g. a 24 Hz mode) go to the wire. 

The solution is to disable scaling: 

    xrandr --output DVI-I-1 --set 'scaling mode' None

Instead of DVI-I-1, use the output you have. 

### Starting with 3.13, VBIOS unknown opcode errors prevent module load

If you see a message along the lines of

    nouveau E[   VBIOS][0000:05:00.0] 0xc581[ ]: unknown opcode 0x00
    nouveau E[ DEVINIT][0000:05:00.0] init failed, -22
    nouveau E[     DRM] failed to create 0x80000080, -22

This most likely means that the VBIOS is being read from the wrong location, usually the PCIROM method. Take a look at the earlier messages and look for a method that was rejected due to "checksum invalid". If there is such a method (e.g. PRAMIN), add ``nouveau.config=NvBios=PRAMIN`` to your kernel command line (or as a module option). [Obviously replace PRAMIN with whatever method was the one that gave the "checksum invalid" message.]
